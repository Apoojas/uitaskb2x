import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router } from '@angular/router';
import { CallapiService } from "./shared/call-api.service";
import { ToastrService } from 'ngx-toastr';

declare let $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('cat_selected') cat_selected;
  logginUser: number = 3;
  loginUserObj: any;
  loginUserArray: any = [];
  characters: any;
  resp: any;
  showForm: any = true;
  coordinators: any;
  titles: any;
  counter: any = 0;
  event = {
    title: '',
    description: '',
    category_id: 0,
    paid_event: 'free',
    event_fee: '',
    reward: '',
    date: '', // YYYY-MM-DDTHH:mm (example: 2018-01-19T15:15)
    time: '', 
    duration: 1, // in seconds
    coordinator: [],
    email:''

  };

  currentDate;

  successObject = {};

  constructor(
    public callapiService: CallapiService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentDate = new Date();
    // this.currentDate = new Date(this.currentDate.setDate(this.currentDate.getDate()+1));
    this.fetchUsers();
    this.fetchResponsibilities();
    this.fetchCoordinators();
    this.fetchTitles();
    // $('.selectpicker').selectpicker();
    // $('.submit-btn').click(function(){
    //   console.log("here");
    //   alert('check');
    //    });
    // const promise = new Promise(function(resolve, reject) {
    //   setTimeout(function() {
    //     resolve('Promise returns after 1.5 second!');
    //   }, 1500);
    // });
    
    // promise.then(function(value) {
    //   console.log(value);
    //   // Promise returns after 1.5 second!
    // });
  }

  showMessage(message, type){
    if(type=='success'){
      this.toastr.success(message)
    }else{
      this.toastr.error(message)
    }
  }

  fetchUsers() {
    return this.callapiService.getCharacters().subscribe((res: {}) => {
      this.characters = res;
      // console.log("test"+JSON.stringify(this.Characters));
    })
  }

  fetchTitles() {
    return this.callapiService.getTitles().subscribe((res: {}) => {
      this.titles = res;
      // console.log("test"+JSON.stringify(this.Characters));
    })
  }

  fetchResponsibilities() {
    return this.callapiService.getResponsibilities().subscribe((res: {}) => {
      this.resp = res;
      // console.log("test"+JSON.stringify(this.resp));
    })
  }

  fetchCoordinators() {
    return this.callapiService.getCoordinators().subscribe((res: {}) => {
      this.coordinators = res;
      for(var i = 0 ; i< this.coordinators.length; i++){
        if(this.coordinators[i].id == 3){
          this.logginUser = 3;
          this.coordinators[i]['group'] = 'Me';
          this.coordinators[i]['disabled'] = true;
          this.loginUserObj = this.coordinators[i];
          // this.coordinators.splice(i,1);
        }else{
          this.coordinators[i]['group'] = 'others';
        }
      }
      this.coordinators.splice(3,1);
      this.coordinators = [this.loginUserObj, ...this.coordinators];
      // this.event.coordinator = this.loginUserObj['id'];
      this.event.coordinator = [{"id":3,"name":"Walter","lastname":"Nelson","email":"walter.nelson@hussa.rs","group":"Me"}];
      // this.cat_selected.select(this.loginUserObj['id']);
      // console.log("test"+JSON.stringify(this.coordinators));
    })
  }

  onSubmit(){
    // console.log(JSON.stringify(this.event));
    if(this.event.title == '' || this.event.description == '' || this.event.date == '' || this.event.category_id == null){
      this.showMessage('Enter Valid Details','error')
    }else{
      // console.log(JSON.stringify(this.event));
      this.showForm = false;
      this.successObject['title'] = this.event.title;
      this.successObject['description'] = this.event.description;
      this.successObject['category_id'] = this.event.category_id;
      this.successObject['paid_event'] = this.event.paid_event == 'free' ? false : true;
      this.successObject['event_fee'] = this.event.paid_event == 'free' ? 0 : this.event.event_fee;
      this.successObject['reward'] = this.event.reward;
      this.successObject['date'] = (this.event.date + 'T' + this.event.time);
      this.successObject['duration'] = this.event.duration * 60;
      this.successObject['coordinator'] =  this.event.coordinator;
      console.log("Succ"+JSON.stringify(this.successObject));
    }
  }
  
  validateEmail(){
    // console.log(this.event.email);
    var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    var emailValid = regexp.test(this.event.email);

    // console.log(emailValid)
    if(!emailValid){
      this.showMessage('Invaid Email ID', 'error');
      setTimeout(() => {
        this.event.email='';
        
      }, 500);
    }
  }

  checkLength(){
    setTimeout(() => {
      this.counter = 0 + this.event.description.length;
      if(this.counter>140){
        this.counter = 140;
        this.event.description = this.event.description.substring(0, 140);
      }
    }, 100);
  }

  checkValidation(field){
    // console.log('this.event[field]::'+this.event[field]+':::');
    setTimeout(() => {
      if(this.event[field] == ''){
        this.showMessage('Enter '+field,'error');
      }   
    }, 10);
   
  }

  checkTitle(){
    // console.log('inside');
    if(this.event.title != ''){
      var exists = this.titles.some(i => i.title.includes(this.event.title));

      if(exists){
        this.event.title = '';
        this.showMessage("Title Already exists",'error');
      }
    }
    
  }
}


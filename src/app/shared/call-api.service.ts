import { Injectable } from '@angular/core';

import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class Title {
  userId: number;
  id: number;
  title: string;
}

export class Responsibilities {
  id: number;
  name: string;
}

export class Coordinators {
  id: number;
  name: string;
  lastname: string;
  email: string;
}

export class Character {
  char_id: number;
  name: string;
  birthday: string;
  occupation: [];
  img: string;
  status: string;
  nickname: string;
  appearance: string;
  portrayed: string;
  category: string;
  better_call_saul_appearance: []
}

@Injectable({
  providedIn: 'root'
})

export class CallapiService {

  // REST API
  endpoint = 'http://localhost:4200';

  constructor(private httpClient: HttpClient) { }

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  getCharacters(): Observable<Character> {
    return this.httpClient.get<Character>('https://www.breakingbadapi.com/api/characters')
    .pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  getTitles(): Observable<Title> {
    return this.httpClient.get<Title>('http://www.mocky.io/v2/5bcdd8732f00007300c855da')
    .pipe(
      retry(1),
      catchError(this.processError)
    )
  }


  getResponsibilities(): Observable<Responsibilities> {
    return this.httpClient.get<Responsibilities>('http://www.mocky.io/v2/5bcdd3942f00002c00c855ba')
    .pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  getCoordinators(): Observable<Coordinators> {
    return this.httpClient.get<Coordinators>('http://www.mocky.io/v2/5bcdd7992f00006300c855d5')
    .pipe(
      retry(1),
      catchError(this.processError)
    )
  }


  processError(err) {
     let message = '';
     if(err.error instanceof ErrorEvent) {
      message = err.error.message;
     } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
     }
     console.log(message);
     return throwError(message);
  }
  
}